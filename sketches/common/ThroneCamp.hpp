#ifndef MODULE_PRODUCTION
#define MODULE_PRODUCTION

#include <myEEPROM.h>
#include <myTeams.h>
#include <myFrame.h>
#include <myUpgrades.h>

#define PRODUCTION
#ifdef PRODUCTION
    const unsigned short ACTIVITY_PERIOD_1 = 30000; //turēšanas ilgums capture
    const unsigned short ACTIVITY_PERIOD_2 = 30000; //turēšanas ilgums collect
    const unsigned short ACTIVITY_PERIOD_3 = 30000; //turēšanas ilgums loot
    const unsigned short ACTIVITY_PERIOD_4 = 4000; //turēšanas ilgums upgrade camp
    const unsigned long ACTIVITY_PERIOD_5 = 12*60000; //laiks līdz capture
    const long CYCLE_LENGTH = 80L * 60000;
    const long COLLECT_AFTER_COLLECT_INTERVAL = 35L * 60000;
    const long COLLECT_AFTER_COLLECT_RANDOM = 20L * 60000;
    const long LOOT_AFTER_LOOT_INTERVAL = 12L * 60000; //Interval after you can loot again base
    const long LOOT_AFTER_LOOT_RANDOM = 6L * 60000; //Interval after you can loot again additional random
    const long READY_AFTER_LOOT_INTERVAL = 12L * 60000; //Interval after loot after which you can operate box again
    const long PRODUCTION_AFTER_LOOT_INTERVAL = 4L * 60000; //Interval for which resources are not produced
    const short INITIAL_RESOURCES_AT_GAME_START = 4;
    const long RANDOM_SEED_END = 1100;
#else
    const unsigned short ACTIVITY_PERIOD_1 = 5000; //turēšanas ilgums capture
    const unsigned short ACTIVITY_PERIOD_2 = 6000; //turēšanas ilgums collect
    const unsigned short ACTIVITY_PERIOD_3 = 4000; //turēšanas ilgums loot
    const unsigned short ACTIVITY_PERIOD_4 = 3000; //turēšanas ilgums upgrade camp
    const unsigned long ACTIVITY_PERIOD_5 = 20000; //laiks līdz capture
    const long CYCLE_LENGTH = 80L * 1000;
    const long COLLECT_AFTER_COLLECT_INTERVAL = 45L * 1000;
    const long COLLECT_AFTER_COLLECT_RANDOM = 30L * 60000;
    const long LOOT_AFTER_LOOT_INTERVAL = 7L * 1000; //Interval after you can loot again base
    const long LOOT_AFTER_LOOT_RANDOM = 7L * 1000; //Interval after you can loot again additional random
    const long READY_AFTER_LOOT_INTERVAL = 20L * 1000;
    const long PRODUCTION_AFTER_LOOT_INTERVAL = 5L * 1000;
    const short INITIAL_RESOURCES_AT_GAME_START = 8;
    const long RANDOM_SEED_END = 1100;
#endif

enum class ThroneCampStatus : int {
    OK = 0,
    NotReady = -1,
    NotOwned = -2,
    AlreadyOwned = -3,
    NothingToCollect = -4,
    RecentlyLooted = -5,
    AlreadyUpgraded = -6,
    AlreadyMaxUpgrades = -7,
    WrongUpgradeCode = -8,
    AlradyCapturing = -9,
    CaptureImprogress = -10,
    LiberationImprogress = -11
};

struct Response {
    ThroneCampStatus status;
    const char* text;
    int value;
};

class ThroneCamp {
public:
    ThroneCamp(const MyFrame& frame);
    void setup();
    Response finishCapture();
    Response startCapture(Team team, bool dryrun = false);
    Response cancleCapture();
    Response capture(Team team);
    Response collect(Team team, bool dryrun = false);
    Response loot(Team team, bool dryrun = false);
    Response upgrade(Team team, const char* code, bool dryrun = false);

    const char* getName();
    const char* getResource();
    Team getOwner();
    Team getCapturer();
    long getCaptureIn();
    char isCapturing();
    long getCollectIn();
    long getLootIn();
    long getProduceIn();
    void setStockpile(Team team, short stockpile);
    void setLooted(Team team, long produce=PRODUCTION_AFTER_LOOT_INTERVAL, long ready=READY_AFTER_LOOT_INTERVAL);
    float getProductionSpeed();
    float getStockpile();
    float predictStockpile(long future_time);
    char getUpgrades();
    short getUpgradeCount();
    void getUpgradeCode(short code, char* buffer, byte length);
    void setUpgrades(Team team, short id);
    void setStartingCollectCooldown(Team team);
    void setId(unsigned char type);
    unsigned char getId();
    ThroneCampType getType();
    ThroneCampType getType(unsigned char id);

protected:
    unsigned char _id;
    const MyFrame& _frame;  // Ampersand stores by reference instead of a copy.
    unsigned long _last_timestamp;
    float _last_stockpile;
    Team _owner;
    long _collect_alarm;
    long _loot_alarm;
    long _ready_alarm;
    float _production_speed;
    short _upgrades;
    MyEEPROM _eeprom;
    bool _is_replaying_logs;
    long _emulatedClockTime;
    char _active_capture;
    long _capture_alarm;
    Team _capturer;
    long _getClockTime();
    void _appendLog(LogEntry log) const;
};

#endif
