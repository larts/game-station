#include "myInput.h"


MyInput::MyInput() {
    pinMode((uint8_t)MY_PINS::Team_Purple, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Team_Pink, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Team_Gold, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Team_Red, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Team_Green, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Team_Blue, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Team_Orange, INPUT_PULLUP);

    pinMode((uint8_t)MY_PINS::Action_1, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Action_2, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Action_3, INPUT_PULLUP);
    pinMode((uint8_t)MY_PINS::Action_4, INPUT_PULLUP);

    pinMode(A7, INPUT); //battery voltage measurment
}

Buttons lastKeyPress = {
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    (MY_PINS)0,

    false,
    false,
    false,
    false,
    (MY_PINS)0,

    0,
    0,
    0,
};

/**
 * Reads all buttons. Call regularly, once per frame.
 */
void MyInput::read() {

    keysPress = {
        !digitalRead((uint8_t)MY_PINS::Team_Purple),  //invertē lasījumu, jo normally open=TRUE aka 1
        !digitalRead((uint8_t)MY_PINS::Team_Blue),
        !digitalRead((uint8_t)MY_PINS::Team_Green),
        !digitalRead((uint8_t)MY_PINS::Team_Red),
        !digitalRead((uint8_t)MY_PINS::Team_Orange),
        !digitalRead((uint8_t)MY_PINS::Team_Gold),
        !digitalRead((uint8_t)MY_PINS::Team_Pink),
        (MY_PINS)0,
        !digitalRead((uint8_t)MY_PINS::Action_1),
        !digitalRead((uint8_t)MY_PINS::Action_2),
        !digitalRead((uint8_t)MY_PINS::Action_3),
        !digitalRead((uint8_t)MY_PINS::Action_4),
        (MY_PINS)0,
        0,
        0,
        0,
    };

    if (keysPress.Team_Purple) { keysPress.Team = MY_PINS::Team_Purple; }
    else if (keysPress.Team_Blue) { keysPress.Team = MY_PINS::Team_Blue; }
    else if (keysPress.Team_Green) { keysPress.Team = MY_PINS::Team_Green; }
    else if (keysPress.Team_Red) { keysPress.Team = MY_PINS::Team_Red; }
    else if (keysPress.Team_Orange) { keysPress.Team = MY_PINS::Team_Orange; }
    else if (keysPress.Team_Gold) { keysPress.Team = MY_PINS::Team_Gold; }
    else if (keysPress.Team_Pink) { keysPress.Team = MY_PINS::Team_Pink; }

    if (keysPress.Action_1) { keysPress.Action = MY_PINS::Action_1; }
    else if (keysPress.Action_2) { keysPress.Action = MY_PINS::Action_2; }
    else if (keysPress.Action_3) { keysPress.Action = MY_PINS::Action_3; }
    else if (keysPress.Action_4) { keysPress.Action = MY_PINS::Action_4; }

    keysPress.CRC_Team = keysPress.Team_Purple
        + (keysPress.Team_Blue << 1)
        + (keysPress.Team_Green << 2)
        + (keysPress.Team_Red << 3)
        + (keysPress.Team_Orange << 4)
        + (keysPress.Team_Gold << 5)
        + (keysPress.Team_Pink << 6);
    keysPress.CRC_Action = keysPress.Action_1
        + (keysPress.Action_2 << 1)
        + (keysPress.Action_3 << 2)
        + (keysPress.Action_4 << 3);
    keysPress.CRC = keysPress.CRC_Team + (keysPress.CRC_Action << 7);


    keysDown = {
        !lastKeyPress.Team_Purple && keysPress.Team_Purple,
        !lastKeyPress.Team_Blue && keysPress.Team_Blue,
        !lastKeyPress.Team_Green && keysPress.Team_Green,
        !lastKeyPress.Team_Red && keysPress.Team_Red,
        !lastKeyPress.Team_Orange && keysPress.Team_Orange,
        !lastKeyPress.Team_Gold && keysPress.Team_Gold,
        !lastKeyPress.Team_Pink && keysPress.Team_Pink,
        (MY_PINS)0,
        !lastKeyPress.Action_1 && keysPress.Action_1,
        !lastKeyPress.Action_2 && keysPress.Action_2,
        !lastKeyPress.Action_3 && keysPress.Action_3,
        !lastKeyPress.Action_4 && keysPress.Action_4,
        (MY_PINS)0,
        0,
        0,
        0,
    };

    if (keysDown.Team_Purple) { keysDown.Team = MY_PINS::Team_Purple; }
    else if (keysDown.Team_Blue) { keysDown.Team = MY_PINS::Team_Blue; }
    else if (keysDown.Team_Green) { keysDown.Team = MY_PINS::Team_Green; }
    else if (keysDown.Team_Red) { keysDown.Team = MY_PINS::Team_Red; }
    else if (keysDown.Team_Orange) { keysDown.Team = MY_PINS::Team_Orange; }
    else if (keysDown.Team_Gold) { keysDown.Team = MY_PINS::Team_Gold; }
    else if (keysDown.Team_Pink) { keysDown.Team = MY_PINS::Team_Pink; }

    if (keysDown.Action_1) { keysDown.Action = MY_PINS::Action_1; }
    else if (keysDown.Action_2) { keysDown.Action = MY_PINS::Action_2; }
    else if (keysDown.Action_3) { keysDown.Action = MY_PINS::Action_3; }
    else if (keysDown.Action_4) { keysDown.Action = MY_PINS::Action_4; }

    keysDown.CRC_Team = keysDown.Team_Purple
        + (keysDown.Team_Blue << 1)
        + (keysDown.Team_Green << 2)
        + (keysDown.Team_Red << 3)
        + (keysDown.Team_Orange << 4)
        + (keysDown.Team_Gold << 5)
        + (keysDown.Team_Pink << 6);
    keysDown.CRC_Action = keysDown.Action_1
        + (keysDown.Action_2 << 1)
        + (keysDown.Action_3 << 2)
        + (keysDown.Action_4 << 3);
    keysDown.CRC = keysDown.CRC_Team + (keysDown.CRC_Action << 7);


    keysUp = {
        lastKeyPress.Team_Purple && !keysPress.Team_Purple,
        lastKeyPress.Team_Blue && !keysPress.Team_Blue,
        lastKeyPress.Team_Green && !keysPress.Team_Green,
        lastKeyPress.Team_Red && !keysPress.Team_Red,
        lastKeyPress.Team_Orange && !keysPress.Team_Orange,
        lastKeyPress.Team_Gold && !keysPress.Team_Gold,
        lastKeyPress.Team_Pink && !keysPress.Team_Pink,
        (MY_PINS)0,
        lastKeyPress.Action_1 && !keysPress.Action_1,
        lastKeyPress.Action_2 && !keysPress.Action_2,
        lastKeyPress.Action_3 && !keysPress.Action_3,
        lastKeyPress.Action_4 && !keysPress.Action_4,
        (MY_PINS)0,
        0,
        0,
        0,
    };

    if (keysUp.Team_Purple) { keysUp.Team = MY_PINS::Team_Purple; }
    else if (keysUp.Team_Blue) { keysUp.Team = MY_PINS::Team_Blue; }
    else if (keysUp.Team_Green) { keysUp.Team = MY_PINS::Team_Green; }
    else if (keysUp.Team_Red) { keysUp.Team = MY_PINS::Team_Red; }
    else if (keysUp.Team_Orange) { keysUp.Team = MY_PINS::Team_Orange; }
    else if (keysUp.Team_Gold) { keysUp.Team = MY_PINS::Team_Gold; }
    else if (keysUp.Team_Pink) { keysUp.Team = MY_PINS::Team_Pink; }

    if (keysUp.Action_1) { keysUp.Action = MY_PINS::Action_1; }
    else if (keysUp.Action_2) { keysUp.Action = MY_PINS::Action_2; }
    else if (keysUp.Action_3) { keysUp.Action = MY_PINS::Action_3; }
    else if (keysUp.Action_4) { keysUp.Action = MY_PINS::Action_4; }

    keysDown.CRC_Team = keysDown.Team_Purple
        + (keysDown.Team_Blue << 1)
        + (keysDown.Team_Green << 2)
        + (keysDown.Team_Red << 3)
        + (keysDown.Team_Orange << 4)
        + (keysDown.Team_Gold << 5)
        + (keysDown.Team_Pink << 6);
    keysDown.CRC_Action = keysDown.Action_1
        + (keysDown.Action_2 << 1)
        + (keysDown.Action_3 << 2)
        + (keysDown.Action_4 << 3);
    keysDown.CRC = keysDown.CRC_Team + (keysDown.CRC_Action << 7);


    lastKeyPress = keysPress;
};