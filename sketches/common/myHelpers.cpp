#include "myHelpers.h"

byte getSeconds(long time_ms) {
    return (time_ms / 1000) % 60;
}
byte getMinutes(long time_ms) {
    return (time_ms / 1000 / 60) % 60;
}
byte getHours(long time_ms) {
    return time_ms / 1000 / 60 / 60;
}
void formatHMS(char* buffer, byte length, long time_ms) {
    long time_ms_abs = time_ms >= 0 ? time_ms : -time_ms;
    byte offset = 0;

    if (time_ms < 0) {
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = '-';
        offset++;
    }

    char num_buffer[4];
    num_buffer[3] = 0;

    // Hours
    itoa(getHours(time_ms_abs), num_buffer, 10);

    bool not_zero = !(num_buffer[0] == '0' && num_buffer[1] == 0);
    if (not_zero) {
        for (byte i=0; i<3; i++) {
            if (num_buffer[i] == 0) break;
            if (length - offset == 1) { buffer[offset] = 0; return; }
            buffer[offset] = num_buffer[i];
            offset++;
        }
        if (length - offset == 0) { buffer[offset] = 0; return; }
        buffer[offset] = 'h';
        offset++;
    }

    // Minutes
    itoa(getMinutes(time_ms_abs), num_buffer, 10);

    not_zero |= !(num_buffer[0] == '0' && num_buffer[1] == 0);
    if (not_zero) {
        for (byte i=0; i<3; i++) {
            if (num_buffer[i] == 0) break;
            if (length - offset == 1) { buffer[offset] = 0; return; }
            buffer[offset] = num_buffer[i];
            offset++;
        }
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = 'm';
        offset++;
    }

    // Seconds
    itoa(getSeconds(time_ms_abs), num_buffer, 10);

    for (byte i=0; i<3; i++) {
        if (num_buffer[i] == 0) break;
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = num_buffer[i];
        offset++;
    }
    if (length - offset == 1) { buffer[offset] = 0; return; }
    buffer[offset] = 's';
    offset++;

    buffer[offset] = 0;
}

void formatColons(char* buffer, byte length, long time_ms, bool sign) {
    long time_ms_abs = time_ms >= 0 ? time_ms : -time_ms;
    byte offset = 0;

    if (sign) {
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = time_ms < 0 ? '-' : ' ';
        offset++;
    }

    char num_buffer[4];
    num_buffer[3] = 0;

    // Hours
    itoa(getHours(time_ms_abs), num_buffer, 10);
    if (num_buffer[1] == 0) {
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = '0';
        offset++;
    }

    for (byte i=0; i<3; i++) {
        if (num_buffer[i] == 0) break;
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = num_buffer[i];
        offset++;
    }
    if (length - offset == 1) { buffer[offset] = 0; return; }
    buffer[offset] = ':';
    offset++;

    // Minutes
    itoa(getMinutes(time_ms_abs), num_buffer, 10);
    if (num_buffer[1] == 0) {
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = '0';
        offset++;
    }

    for (byte i=0; i<3; i++) {
        if (num_buffer[i] == 0) break;
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = num_buffer[i];
        offset++;
    }
    if (length - offset == 1) { buffer[offset] = 0; return; }
    buffer[offset] = ':';
    offset++;

    // Seconds
    itoa(getSeconds(time_ms_abs), num_buffer, 10);
    if (num_buffer[1] == 0) {
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = '0';
        offset++;
    }

    for (byte i=0; i<3; i++) {
        if (num_buffer[i] == 0) break;
        if (length - offset == 1) { buffer[offset] = 0; return; }
        buffer[offset] = num_buffer[i];
        offset++;
    }

    buffer[offset] = 0;
}
