#if defined(EPOXY_CORE_ESP8266)
    #include <EpoxyEepromEsp.h>
    #define EEPROM EpoxyEepromEspInstance
#else
    #include <EpoxyEepromAvr.h>
    #define EEPROM EpoxyEepromAvrInstance
#endif
