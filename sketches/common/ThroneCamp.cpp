#include "ThroneCamp.hpp"

ThroneCamp::ThroneCamp(const MyFrame& frame) :
    _frame(frame),
    _last_timestamp(0),  // At the start of the game..
    _last_stockpile(0),  // .. with empty stockpile.
    _owner(team_ADMIN),
    _upgrades(0b0000),
    _collect_alarm(0),
    _loot_alarm(0),
    _ready_alarm(0),
    _production_speed(8),
    _is_replaying_logs(0),
    _emulatedClockTime(0),
    _active_capture(0),
    _capture_alarm(INT32_MIN),
    _capturer(team_ADMIN)
{
    _eeprom = MyEEPROM();
    setId(_eeprom.getId());
    srand(getId()+RANDOM_SEED_END);
}

void ThroneCamp::setup() {
    // Basically, emulate the clock and replay all logs.
    _is_replaying_logs = true;
        #ifdef USE_SERIAL
        Serial.println("HB,Event,Team,Value");
        #endif
    for (short i=0; i<_eeprom.getLogCount(); i++) {
        LogEntry log = _eeprom.getLog(i);
        _emulatedClockTime = _frame.fromHeartbeat(log.heartbeat);
        #ifdef USE_SERIAL
            Serial.println(String(log.heartbeat) + "," + String((unsigned char)log.type) + "," + getTeam(log.team).name + ","+ String(log.value));
        #endif
        switch(log.type) {
            case(EventType::StartCapture): { startCapture(getTeam(log.team)); break; }
            case(EventType::CancelCapture): { cancleCapture(); break; }
            case(EventType::Capture): { capture(getTeam(log.team)); break; }
            case(EventType::Collect): { collect(getTeam(log.team)); break; }
            case(EventType::Loot): { loot(getTeam(log.team)); break; }
            case(EventType::Upgrade): { setUpgrades(getTeam(log.team), log.value); break; }
            case(EventType::SetStockpile): { setStockpile(getTeam(log.team), log.value); break; }
            case(EventType::SetStartingCollectCooldown): { setStartingCollectCooldown(getTeam(log.team)); break; }
        }
    }
    _emulatedClockTime = 0;
    _is_replaying_logs = false;
}

const char* ThroneCamp::getName() {
    switch(getType()) {
        case(ThroneCampType::Forest): return "Forest";
        case(ThroneCampType::Farm): return "Farm";
        case(ThroneCampType::Mine): return "Mine";
    }
}
const char* ThroneCamp::getResource() {
    switch(getType()) {
        case(ThroneCampType::Forest): return "Wood";
        case(ThroneCampType::Farm): return "Food";
        case(ThroneCampType::Mine): return "Iron";
    }
}
Response ThroneCamp::startCapture(Team team, bool dryrun) {
    if(team.pin == _owner.pin && !_active_capture) {
        return {ThroneCampStatus::AlreadyOwned,"",0};
    }

    if(team.pin == _capturer.pin && _active_capture) {
        return {ThroneCampStatus::AlradyCapturing,"",0};
    }

    if(!dryrun) {
        if(team.pin == _owner.pin) {
            return cancleCapture();
        } else {
            _active_capture = (team.pin == team_ADMIN.pin ? 2 : 1);
            _capturer = team;
            _capture_alarm = _getClockTime() + ACTIVITY_PERIOD_5;
            _appendLog({EventType::StartCapture, team.pin, _frame.getHeartbeat(), 0});
        }
    }
    return {ThroneCampStatus::OK,"",0};
}
Response ThroneCamp::cancleCapture() {
    _active_capture = 0;
    _capturer = team_ADMIN;
    _capture_alarm = INT32_MIN;
    _appendLog({EventType::CancelCapture, _owner.pin, _frame.getHeartbeat(), 0});
    return {ThroneCampStatus::OK,"",0};
}
Response ThroneCamp::finishCapture() {
    _owner = _capturer;
    _active_capture = 0;
    _capture_alarm = INT32_MIN;
    _capturer = team_ADMIN;
    _appendLog({EventType::Capture, _owner.pin, _frame.getHeartbeat(), 0});
    return {ThroneCampStatus::OK,"",0};
}
Response ThroneCamp::capture(Team team) {
    _owner = team;
    _active_capture = 0;
    _capture_alarm = INT32_MIN;
    _capturer = team_ADMIN;
    _appendLog({EventType::Capture, team.pin, _frame.getHeartbeat(), 0});
    #ifdef USE_SERIAL
    // Serial.println("Capture HB: " + String(_getClockTime()) + " team: " + team.name);
    #endif
    return {ThroneCampStatus::OK,"",0};
}
Response ThroneCamp::collect(Team team, bool dryrun) {
    if(_collect_alarm > _getClockTime()) {
        #ifdef USE_SERIAL
        // Serial.println("Collect HB: " + String(_getClockTime()) + " Error: Not ready to collect");
        #endif
        return {ThroneCampStatus::NotReady,"",0};
    }
    if(team.pin != _owner.pin) {
        #ifdef USE_SERIAL
        // Serial.println("Collect HB: " + String(_getClockTime()) + " Error: Not right team, need " + team.name + " have " + _owner.name);
        #endif
        return {ThroneCampStatus::NotOwned,"",0};
    }
    if(_active_capture == 1) {
        #ifdef USE_SERIAL
        // Serial.println("Collect HB: " + String(_getClockTime()) + " Error: Capture in progress");
        #endif
        return {ThroneCampStatus::CaptureImprogress,"",0};
    }
    if(_active_capture == 2) {
        #ifdef USE_SERIAL
        // Serial.println("Collect HB: " + String(_getClockTime()) + " Error: Liberate in progress");
        #endif
        return {ThroneCampStatus::LiberationImprogress,"",0};
    }
    int collected = int(getStockpile());
    if(collected <= 0) {
        #ifdef USE_SERIAL
        // Serial.println("Collect HB: " + String(_getClockTime()) + " Error: Nothing to collect");
        #endif
        return {ThroneCampStatus::NothingToCollect,"",0};
    }

    if(!dryrun) {
        _last_stockpile = getStockpile() - collected;
        _last_timestamp = max(_last_timestamp,(long unsigned)_getClockTime());
        _collect_alarm = _getClockTime() + COLLECT_AFTER_COLLECT_INTERVAL + ((float)rand()/RAND_MAX) * COLLECT_AFTER_COLLECT_RANDOM;
        _appendLog({EventType::Collect, team.pin, _frame.getHeartbeat(), short(collected)});
        #ifdef USE_SERIAL
        // Serial.println("HB: " + String(_getClockTime()) + " Good: successull collect");
        #endif
    }

    return {ThroneCampStatus::OK,"",collected};
}
Response ThroneCamp::loot(Team team, bool dryrun) {
    if(_ready_alarm > _getClockTime()) {
        return {ThroneCampStatus::RecentlyLooted,"",0};
    }
    if(_loot_alarm > _getClockTime()) {
        #ifdef USE_SERIAL
        // Serial.println("Loot HB: " + String(_getClockTime()) + " Error: Not ready to loot");
        #endif
        return {ThroneCampStatus::NotReady,"",0};
    }
    short collected = 0;
    if (int(getStockpile()) <= 1)
        collected = int(getStockpile());
    else
        collected = 1;
    if(!dryrun) {
        _last_stockpile = getStockpile() - collected;
        _last_timestamp = _getClockTime() + PRODUCTION_AFTER_LOOT_INTERVAL;
        _loot_alarm = _getClockTime() + LOOT_AFTER_LOOT_INTERVAL + ((float)rand()/RAND_MAX) * LOOT_AFTER_LOOT_RANDOM;
        _ready_alarm = _getClockTime() + READY_AFTER_LOOT_INTERVAL;
        _appendLog({EventType::Loot, team.pin, _frame.getHeartbeat(), collected});
    }
    return {ThroneCampStatus::OK,"",collected};
}
void ThroneCamp::setLooted(Team team, long produce, long ready) {
    _last_stockpile = getStockpile();
    _last_timestamp = _getClockTime() + produce;
    _ready_alarm = _getClockTime() + ready;
}
Response ThroneCamp::upgrade(Team team, const char* code, bool dryrun) {
    if(!dryrun){
        for (byte i = 0; i < UPGRADE_CODE_COUNT; i++) {
            if (strcmp(code, upgrade_codes[i].Upgrade) != 0) continue;

            if (_upgrades & (0b0001<<i)) {
                return {ThroneCampStatus::AlreadyUpgraded,upgrade_codes[i].Reward,0};
            }

            _upgrades = _upgrades | (0b0001<<i);
            _last_stockpile = getStockpile();
            _last_timestamp = max(_last_timestamp,(long unsigned)_getClockTime());
            _appendLog({EventType::Upgrade, team.pin, _frame.getHeartbeat(), (short)i});

            return {ThroneCampStatus::OK,upgrade_codes[i].Reward,0};
        }
        return {ThroneCampStatus::WrongUpgradeCode,"",0};
    }
    return {ThroneCampStatus::OK,"",0};
}
void ThroneCamp::setUpgrades(Team team, short id) {
    _upgrades = _upgrades ^ (0b0001<<id);
    _appendLog({EventType::Upgrade, team.pin, _frame.getHeartbeat(), id});
}
void ThroneCamp::getUpgradeCode(short code, char* buffer, byte length){
    byte offset = 0;

    for (byte i = 0; i<length; i++) {
        if (length - offset == 1) { buffer[offset] = 0; return; }
        if (upgrade_codes[code].Upgrade[i] == 0) break;
        buffer[i] = upgrade_codes[code].Upgrade[i];
        offset++;
    }

    if (length - offset == 1) { buffer[offset] = 0; return; }
    buffer[offset] = '-';

    for (byte i = 0; i<(length - offset); i++) {
        if (length - offset == 1) { buffer[offset] = 0; return; }
        if (upgrade_codes[code].Reward[i] == 0) break;
        buffer[i] = upgrade_codes[code].Reward[i];
        offset++;
    }

    buffer[offset] = 0;
}
char ThroneCamp::isCapturing(){
    return _active_capture;
}
long ThroneCamp::getCaptureIn() {
    return _capture_alarm;
}
long ThroneCamp::getCollectIn() {
    return max((long)0, _collect_alarm - _getClockTime());
}
long ThroneCamp::getLootIn() {
    return max((long)0, _ready_alarm - _getClockTime());
}
long ThroneCamp::getProduceIn() {
    return max((long)0, (long)(_last_timestamp - _getClockTime()));
}
void ThroneCamp::setStockpile(Team team, short stockpile) {
    _last_timestamp = _getClockTime();
    _last_stockpile = stockpile;
    _appendLog({EventType::SetStockpile, team.pin, _frame.getHeartbeat(), stockpile});
}
float ThroneCamp::getProductionSpeed() {
    short bonus = (getUpgradeCount() >= 2) ? 2 : getUpgradeCount();
    return (_production_speed + (bonus * 1)) / CYCLE_LENGTH;
}
float ThroneCamp::getStockpile() {
    return predictStockpile(_getClockTime());
}
float ThroneCamp::predictStockpile(long future_time) {
    if (future_time < _last_timestamp) return _last_stockpile;

    return _last_stockpile + (future_time - _last_timestamp) * getProductionSpeed();
}
void ThroneCamp::setStartingCollectCooldown(Team team) {
    _collect_alarm = _getClockTime() + (COLLECT_AFTER_COLLECT_INTERVAL + COLLECT_AFTER_COLLECT_RANDOM/2)/2 + ((float)rand()/RAND_MAX) * COLLECT_AFTER_COLLECT_RANDOM;
    _appendLog({EventType::SetStartingCollectCooldown, team.pin, _frame.getHeartbeat(), 0});
}
void ThroneCamp::setId(unsigned char id) {
    _id = id;
    _eeprom.putId(id);
}
unsigned char ThroneCamp::getId() {
    return _id;
}
ThroneCampType ThroneCamp::getType() {
    return getType(_id);
}
ThroneCampType ThroneCamp::getType(unsigned char id) {
    switch (id % 3){
        case 0: return ThroneCampType::Forest;
        case 1: return ThroneCampType::Farm;
        case 2: return ThroneCampType::Mine;
    }
}
Team ThroneCamp::getOwner() {
    return _owner;
}
Team ThroneCamp::getCapturer() {
    return _capturer;
}
char ThroneCamp::getUpgrades() {
    return _upgrades;
}
short ThroneCamp::getUpgradeCount() {
    short count = 0;
    for (int i=0;i<4;i++){
        if (_upgrades & (0b0001<<i))
            count++;
    }
    return count;
}

long ThroneCamp::_getClockTime() {
    if(_emulatedClockTime > 0) {
        return _emulatedClockTime;
    } else {
        return _frame.clocktime_ms;
    }
}

/**
 * Doesn't actually write to memory during replay.
 */
void ThroneCamp::_appendLog(LogEntry log) const {
    if(!_is_replaying_logs && _emulatedClockTime == 0) {
        _eeprom.appendLog(log);
        #ifdef USE_SERIAL
        Serial.println("Log HB: " + String(log.heartbeat) + " Event: " + String((unsigned char)log.type) + " Team: " + getTeam(log.team).name + " Value: " + String(log.value));
        #endif
    };

}
