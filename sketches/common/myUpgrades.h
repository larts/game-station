#ifndef MODULE_MY_UPGRADES
#define MODULE_MY_UPGRADES

#include "myConfig.h"
#include <Arduino.h>

struct Upgrade {
    const char* Upgrade;
    const char* Reward;
};

const byte UPGRADE_CODE_COUNT = 4;

#define TEST

const static Upgrade upgrade_codes[UPGRADE_CODE_COUNT] = {
#ifdef ID1
    {"MASHES","PERCENT"},
    {"TESTATE","FASHION"},
    {"HOTSHOT","THEATER"},
    {"SHOTS","TREASURE"},
#endif
#ifdef ID2
    {"HOOHAS","DILEMMA"},
    {"OSTEOMATA","CONTROL"},
    {"STEMMATA","EYEBROW"},
    {"HAMTOE","CHERRY"},
#endif
#ifdef ID3
    {"STEMMA","SURFACE"},
    {"SMOOTHE","TRAGEDY"},
    {"HEATHS","ADVISER"},
    {"MOSTHOE","PECKISH"},
#endif
#ifdef ID4
    {"TATTOO","REVENGE"},
    {"MESHES","COLLEGE"},
    {"HEMOSTATS","PUDDING"},
    {"METHOSE","PLATIPUS"},
#endif
#ifdef ID5
    {"TESTATES","SUPPORT"},
    {"TOMATOE","CERTAIN"},
    {"SHAMES","SECTION"},
    {"ATMOST","SPECIAL"},
#endif
#ifdef ID6
    {"HOTSEAT","GLACIER"},
    {"HOTSHOE","RELEASE"},
    {"SHEATH","FEATHER"},
    {"OMAMA","STATUE"},
#endif
#ifdef ID7
    {"ASTHMA","WRESTLE"},
    {"ESTATES","HOLIDAY"},
    {"ETHOSES","SCRATCH"},
    {"ATHOME","MIRROR"},
#endif
#ifdef ID8
    {"MOTTES","DESPAIR"},
    {"HAMATES","COMPACT"},
    {"SOOTHES","SPEAKER"},
    {"EATSOME","PIZZA"},
#endif
#ifdef ID9
    {"MAESTOSO","DICTATE"},
    {"STEATOMAS","DIAMOND"},
    {"SMOOTH","VARIANT"},
    {"MOSTHEAT","PORCELAIN"},
#endif
#ifdef ID10
    {"MOTMOTS","SOCIETY"},
    {"STOMATA","EXPLOIT"},
    {"HOMMOS","SPEAKER"},
    {"SHAMOS","WINDOW"},
#endif
#ifdef ID11
    {"OSMOSE","MORNING"},
    {"MESOSOME","DEFICIT"},
    {"TSAMMAS","MONARCH"},
    {"HOTMEAT","SCREEN"},
#endif
#ifdef ID12
    {"SEAMATE","MINIMUM"},
    {"HEMOSTAT","ENHANCE"},
    {"TOMATOES","TRAINER"},
    {"HOSTSHOE","PURIFY"},
#endif
#ifdef ID13
    {"TATTOOS","PERSIST"},
    {"TEASETS","OPINION"},
    {"EMOTES","REALITY"},
    {"MOSEES","TARNISH"},
#endif
#ifdef ID14
    {"SMOOSH","BARGAIN"},
    {"HOMEOSES","WARRANT"},
    {"HOTSHOTS","INCLUDE"},
    {"SOSTOST","PATHING"},
#endif
#ifdef ID15
    {"TOESHOE","HABITAT"},
    {"HEMATOMA","SOLDIER"},
    {"SMOOTHEST","SWEATER"},
    {"HOMIES","SWEET"},
#endif
#ifdef ID16
    {"SHEATHES","LECTURE"},
    {"MOTTOES","STADIUM"},
    {"TEAMMATE","SYMPTOM"},
    {"SHOEHAT","PANIC"},
#endif
#ifdef ID17
    {"MATSAHS","CLARITY"},
    {"HASHES","CHARITY"},
    {"MOSTEST","FORMULA"},
    {"THEHAT","SHOWER"},
#endif
#ifdef ID19
    {"MAMEES","HOSTILE"},
    {"SHEATHE","MESSAGE"},
    {"THEMES","CONTENT"},
    {"THEMOST","STABLE"},
#endif
#ifdef ID22
    {"OSTEOMA","EXPLOIT"},
    {"SHAMMASH","SAUSAGE"},
    {"SOMATA","PEASANT"},
    {"THEMESH","NETWORK"},
#endif
#ifdef ID25
    {"HATTEAM","EXPOSE"},
    {"TOEHOME","BUTTER"},
    {"MESHHOST","FRIDGE"},
    {"SOMESOE","BRIDGE"},
#endif
#ifdef TEST  
    {"TEST","RES1"},
    {"TESTS","RES2"},
    {"TOST","RES3"},
    {"SOMETEST","RES4"},
#endif
};
#endif
