
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <myEEPROM.h>
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
static int i = 5;

MyEEPROM eeprom = MyEEPROM();

void setup()
{

  lcd.init();                      // initialize the lcd
  delay(1000);
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(0,0);
      lcd.print("Clear EEPROM in:");

  for (i; i>0; i--){
    lcd.setCursor(0,1);
    lcd.print(i);
    delay(1000);
    }
  i = 0;

  eeprom.clean();

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Clear EEPROM in: 0");
  lcd.setCursor(0,1);
  lcd.print("COMPLETE!");

}


void loop()
{

}
