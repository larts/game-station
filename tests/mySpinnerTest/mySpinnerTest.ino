#line 2 "mySpinnerTest"

#include <Arduino.h>
#include <AUnit.h>
#include "mySpinner.h"

using aunit::TestRunner;

test(mySpinnerTest, spinner) {
    assertEqual(spinner(), "^");
    assertEqual(spinner(), ">");
    assertEqual(spinner(), "_");
    assertEqual(spinner(), "<");
}

test(mySpinnerTest, progressBar) {
    char buffer[17];

    progressBar(buffer, 0, 100);
    assertEqual(buffer, "|              |");

    progressBar(buffer, 5, 100);
    assertEqual(buffer, "|              |");

    progressBar(buffer, 8, 100);
    assertEqual(buffer, "|=             |");

    progressBar(buffer, 50, 100);
    assertEqual(buffer, "|=======       |");

    progressBar(buffer, 75, 100);
    assertEqual(buffer, "|===========   |");

    progressBar(buffer, 99, 100);
    assertEqual(buffer, "|==============|");

    progressBar(buffer, 100, 100);
    assertEqual(buffer, "|==============|");

    progressBar(buffer, 500, 100);
    assertEqual(buffer, "|==============|");
}

//---------------------------------------------------------------------------

void setup() {
    Serial.begin(9600);
}

void loop() {
    TestRunner::run();
}
