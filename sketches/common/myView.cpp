#include "myView.h"

MyView::MyView(MyLCD &lcd, MyFrame &frame, long default_notification_period) :
    _lcd(lcd),
    _frame(frame),
    _notification_alarm(INT32_MIN),
    _default_notification_period(default_notification_period)
{
    for (byte i=0; i<16; i++) {
        _notification[0][i] = ' ';
        _notification[1][i] = ' ';
    }
}

bool MyView::has_active_notification() {
    return _notification_alarm > _frame.clocktime_ms;
}

void MyView::render_notification() {
    _lcd.printDef(_notification[0], _notification[1]);
}

void MyView::reset_notification_alarm() {
    _notification_alarm = INT32_MIN;
}

void MyView::_notify(const char* line1, const char* line2, long period) {
    _notification_alarm = _frame.clocktime_ms + (period > 0 ? period : _default_notification_period);
    for (byte i=0; i<16; i++) {
        if (line1[i] == 0) break;
        _notification[0][i] = line1[i];
    }
    for (byte i=0; i<16; i++) {
        if (line2[i] == 0) break;
        _notification[1][i] = line2[i];
    }
}


// View template definitions

void MyView::idle(const char* name, long time, long collect_countdown) {
    assign_VT(BLANK, line1);
    assign_VT(BLANK, line2);

    for (byte i=0; i<6; i++) {
        if (name[i] == 0) break;
        line1[i] = name[i];
    }

    const byte COUNTDOWN_LENGTH = 16 - 7 + 1;
    char countdown[COUNTDOWN_LENGTH];
    formatColons(countdown, COUNTDOWN_LENGTH, time);
    for (byte i=0; i<(16 - 7); i++) {
        if (countdown[i] == 0) break;
        line1[7 + i] = countdown[i];
    }

    if (collect_countdown > 0) {
        use_VT(COLLECT_READY);
        for (byte i=0; i<9; i++) {
            line2[i] = COLLECT_READY[i];
        }

        formatHMS(countdown, COUNTDOWN_LENGTH, collect_countdown);
        for (byte i=0; i<(16-9); i++) {
            if (countdown[i] == 0) break;
            line2[9 + i] = countdown[i];
        }
    }

    _lcd.printDef(line1, line2);
}

void MyView::attempt_error(ThroneCampStatus ThroneCampStatusCode) {
    char line1[17], line2[17];

    switch(ThroneCampStatusCode) {
        case(ThroneCampStatus::OK):
            return;
        case(ThroneCampStatus::NotReady): {
            strcpy_P(line1, VT_NOT);
            strcpy_P(line2, VT_READY);
            break;
        }
        case(ThroneCampStatus::NotOwned): {
            strcpy_P(line1, VT_NOT);
            strcpy_P(line2, VT_OWNED);
            break;
        }
        case(ThroneCampStatus::AlreadyOwned): {
            strcpy_P(line1, VT_ALREADY);
            strcpy_P(line2, VT_OWNED);
            break;
        }
        case(ThroneCampStatus::NothingToCollect): {
            strcpy_P(line1, VT_NOTHING);
            strcpy_P(line2, VT_TO_COLLECT);
            break;
        }
        case(ThroneCampStatus::RecentlyLooted): {
            strcpy_P(line1, VT_RECENTLY);
            strcpy_P(line2, VT_REC_LOOTED);
            break;
        }
        case(ThroneCampStatus::AlradyCapturing): {
            strcpy_P(line1, VT_ALREADY);
            strcpy_P(line2, VT_ALR_CAPTURING);
            break;
        }
        case(ThroneCampStatus::CaptureImprogress): {
            strcpy_P(line1, VT_CAPTURE_IN);
            strcpy_P(line2, VT_PROGRESS);
            break;
        }
        case(ThroneCampStatus::LiberationImprogress): {
            strcpy_P(line1, VT_LIBERATION_IN);
            strcpy_P(line2, VT_PROGRESS);
            break;
        }
    }

    _lcd.printDef(line1, line2);
}

void MyView::liberation_cooldown_bar(unsigned long remaining_time, unsigned long interval) {
    use_VT(LIBERATE_DELAY);

    char remaining[16-9];
    formatHMS(remaining, 16-9, 1000 + remaining_time);
    for (byte i=0; i<(16-9); i++) {
        if (remaining[i] == 0) break;
        LIBERATE_DELAY[9 + i] = remaining[i];
    }

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(LIBERATE_DELAY, progress_bar);
}

void MyView::capture_cooldown_bar(unsigned long remaining_time, unsigned long interval) {
    use_VT(CAPTURE_DELAY);

    char remaining[16-9];
    formatHMS(remaining, 16-9, 1000 + remaining_time);
    for (byte i=0; i<(16-9); i++) {
        if (remaining[i] == 0) break;
        CAPTURE_DELAY[9 + i] = remaining[i];
    }

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(CAPTURE_DELAY, progress_bar);
}

void MyView::loot_cooldown_bar(unsigned long remaining_time, unsigned long interval, byte reward, const char* resource) {
    use_VT(LOOT_DELAY);

    char reward_str[4];
    itoa(reward, reward_str, 10);

    byte offset;
    for (byte i=0; i<(16-5); i++) {
        offset = i;
        if (reward_str[i] == 0) break;
        LOOT_DELAY[5 + i] = reward_str[i];
    }

    offset++;

    for (byte i=0; i<(16-5-offset); i++) {
        if (resource[i] == 0) break;
        LOOT_DELAY[5 + offset + i] = resource[i];
    }

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(LOOT_DELAY, progress_bar);
}

void MyView::collect_cooldown_bar(unsigned long remaining_time, unsigned long interval, byte reward, const char* resource) {
    use_VT(COLLECT_DELAY);

    char reward_str[4];
    itoa(reward, reward_str, 10);

    byte offset;
    for (byte i=0; i<(16-8); i++) {
        offset = i;
        if (reward_str[i] == 0) break;
        COLLECT_DELAY[8 + i] = reward_str[i];
    }

    offset++;

    for (byte i=0; i<(16-8-offset); i++) {
        if (resource[i] == 0) break;
        COLLECT_DELAY[8 + offset + i] = resource[i];
    }

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(COLLECT_DELAY, progress_bar);
}

void MyView::upgrade_cooldown_bar(unsigned long remaining_time, unsigned long interval) {
    use_VT(UPGRADE_DELAY);

    char remaining[16-12];
    formatHMS(remaining, 16-12, 1000 + remaining_time);
    for (byte i=0; i<(16-12); i++) {
        if (remaining[i] == 0) break;
        UPGRADE_DELAY[12 + i] = remaining[i];
    }

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(UPGRADE_DELAY, progress_bar);
}

void MyView::capture_progress_bar(unsigned long remaining_time, unsigned long interval) {
    use_VT(CAPTURE_PROG);

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(CAPTURE_PROG, progress_bar);
}

void MyView::liberation_progress_bar(unsigned long remaining_time, unsigned long interval) {
    use_VT(LIBERATE_PROG);

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(LIBERATE_PROG, progress_bar);
}

void MyView::clear_eeprom_safety_bar(unsigned long remaining_time, unsigned long interval) {
    use_VT(EEPROM_WARN);

    char remaining[16-11];
    formatHMS(remaining, 16-11, 1000 + remaining_time);
    for (byte i=0; i<(16-11); i++) {
        if (remaining[i] == 0) break;
        EEPROM_WARN[11 + i] = remaining[i];
    }

    char progress_bar[17];
    progressBar(progress_bar, interval - remaining_time, interval);

    _lcd.printDef(EEPROM_WARN, progress_bar);
}

void MyView::upgrade_code_input(const char* input) {
    use_VT(UPGRADE_CODE);
    assign_VT(BLANK, code_input);

    for (byte i=0; i<16; i++) {
        if (input[i] == 0) break;
        code_input[i] = input[i];
    }

    _lcd.printDef(UPGRADE_CODE, code_input);
}

void MyView::game_not_started(long remaining_time) {
    use_VT(NOT_STARTED);
    use_VT(STARTS_IN);

    char remaining[16-10];
    formatHMS(remaining, 16-10, 1000 + remaining_time);
    for (byte i=0; i<(16-10); i++) {
        if (remaining[i] == 0) break;
        STARTS_IN[10 + i] = remaining[i];
    }

    _lcd.printDef(NOT_STARTED, STARTS_IN);
}

void MyView::camp_is_looted(long remaining_time) {
    use_VT(CAMP_LOOTED);
    use_VT(READY_IN);

    char remaining[16-9];
    formatHMS(remaining, 16-9, 1000 + remaining_time);
    for (byte i=0; i<(16-9); i++) {
        if (remaining[i] == 0) break;
        READY_IN[9 + i] = remaining[i];
    }

    _lcd.printDef(CAMP_LOOTED, READY_IN);
}

void MyView::team_selected(const char* team) {
    use_VT(SELECTED_TEAM);
    assign_VT(BLANK, team_info);

    for (byte i=0; i<16; i++) {
        if (team[i] == 0) break;
        team_info[i] = team[i];
    }

    _lcd.printDef(SELECTED_TEAM, team_info);
}

void MyView::capture_status(const char* owner_name, const char* capturer_name) {
    use_VT(OWNER);
    assign_VT(BLANK, capturer_info);

    for (byte i=0; i<(16-7); i++) {
        if (owner_name[i] == 0) break;
        OWNER[7 + i] = owner_name[i];
    }

    if (capturer_name) {
        use_VT(CAPTURER);

        for (byte i=0; i<6; i++) {
            capturer_info[i] = CAPTURER[i];
        }

        for (byte i=0; i<(16-6); i++) {
            if (capturer_name[i] == 0) break;
            capturer_info[6 + i] = capturer_name[i];
        }
    }

    _lcd.printDef(OWNER, capturer_info);
}

void MyView::collect_status(int amount, const char* resource) {
    use_VT(STOCK);
    use_VT(BLANK);

    const byte STR_BUFFER_LENGTH = 16 - 6 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    str_buffer[STR_BUFFER_LENGTH-1] = 0;
    itoa(amount, str_buffer, 10);

    byte offset = 0;
    for (byte i=0; i<(16-6); i++) {
        offset = i;
        if (str_buffer[i] == 0) break;
        STOCK[6 + i] = str_buffer[i];
    }

    for (byte i=0; i<(16-6-offset); i++) {
        if (resource[i] == 0) break;
        STOCK[6 + offset + i] = resource[i];
    }

    _lcd.printDef(STOCK, BLANK);
}

void MyView::loot_status() {
    use_VT(LOOTING);
    use_VT(BLANK);
    _lcd.printDef(LOOTING, BLANK);
}

void MyView::upgrade_status(byte count, long rate) {
    use_VT(EXPANSIONS);
    assign_VT(PLUS_1_EVERY, rate_info);

    const byte STR_BUFFER_LENGTH = 16 - 6 + 1;
    char str_buffer[STR_BUFFER_LENGTH];

    formatHMS(str_buffer, STR_BUFFER_LENGTH, rate);
    for (byte i=0; i<(16-9); i++) {
        if (str_buffer[i] == 0) break;
        rate_info[9 + i] = str_buffer[i];
    }

    EXPANSIONS[12] = '0' + count;
    _lcd.printDef(EXPANSIONS, rate_info);
}

void MyView::setup_menu() {
    use_VT(SETUP_MENU_1);
    use_VT(SETUP_MENU_2);
    _lcd.printDef(SETUP_MENU_1, SETUP_MENU_2);
}

void MyView::setup_uptime(long clocktime, long uptime) {
    use_VT(CLOCK);
    use_VT(BIRTH);

    const byte TIME_BUFFER_LENGTH = 16 - 7 + 1;
    char time_buffer[TIME_BUFFER_LENGTH];

    formatColons(time_buffer, TIME_BUFFER_LENGTH, clocktime);
    for (byte i=0; i<(16-7); i++) {
        if (time_buffer[i] == 0) break;
        CLOCK[7 + i] = time_buffer[i];
    }

    formatColons(time_buffer, TIME_BUFFER_LENGTH, uptime);
    for (byte i=0; i<(16-7); i++) {
        if (time_buffer[i] == 0) break;
        BIRTH[7 + i] = time_buffer[i];
    }

    _lcd.printDef(CLOCK, BIRTH);
}

void MyView::setup_type(int id, const char* resource, const char* name) {
    use_VT(ID_RESC);
    use_VT(TYPE);

    const byte STR_BUFFER_LENGTH = 3 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(id, str_buffer, 10);

    for (byte i=0; i<3; i++) {
        if (str_buffer[i] == 0) break;
        ID_RESC[3 + i] = str_buffer[i];
    }

    for (byte i=0; i<(16-11); i++) {
        if (resource[i] == 0) break;
        ID_RESC[11 + i] = resource[i];
    }

    for (byte i=0; i<(16-6); i++) {
        if (name[i] == 0) break;
        TYPE[6 + i] = name[i];
    }

    _lcd.printDef(ID_RESC, TYPE);
}

void MyView::fix_menu() {
    use_VT(FIX_MENU_1);
    use_VT(FIX_MENU_2);
    _lcd.printDef(FIX_MENU_1, FIX_MENU_2);
}

void MyView::fix_capture(const char* owner_name) {
    use_VT(INSTA_CAPTURE);
    use_VT(OWNER);

    for (byte i=0; i<(16-7); i++) {
        if (owner_name[i] == 0) break;
        OWNER[7 + i] = owner_name[i];
    }

    _lcd.printDef(INSTA_CAPTURE, OWNER);
}

void MyView::fix_stock(int amount, const char* resource) {
    use_VT(STOCK);
    use_VT(BLANK);

    const byte STR_BUFFER_LENGTH = 16 - 6 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(amount, str_buffer, 10);

    byte offset = 0;
    for (byte i=0; i<(16-6); i++) {
        offset = i;
        if (str_buffer[i] == 0) break;
        STOCK[6 + i] = str_buffer[i];
    }

    for (byte i=0; i<(16-6-offset); i++) {
        if (resource[i] == 0) break;
        STOCK[6 + offset + i] = resource[i];
    }

    _lcd.printDef(STOCK, BLANK);
}

void MyView::fix_loot(long loot_time, long produce_time) {
    use_VT(LOOT);
    use_VT(PRODUCE);

    const byte STR_BUFFER_LENGTH = 16 - 5;
    char str_buffer[STR_BUFFER_LENGTH + 1];

    formatHMS(str_buffer, STR_BUFFER_LENGTH, loot_time);
    for (byte i=0; i<(16-6); i++) {
        if (str_buffer[i] == 0) break;
        LOOT[6 + i] = str_buffer[i];
    }

    formatHMS(str_buffer, STR_BUFFER_LENGTH, produce_time);
    for (byte i=0; i<(16-5); i++) {
        if (str_buffer[i] == 0) break;
        PRODUCE[5 + i] = str_buffer[i];
    }

    _lcd.printDef(LOOT, PRODUCE);
}

void MyView::fix_upgrade(char upgrades) {
    use_VT(BLANK);
    assign_VT(BLANK, upgrade_info);

    for (short i=0; i < 4; i++) {
        upgrade_info[i] = ((0b0001<<i) & upgrades) ? 'U' : 'A';
    }

    _lcd.printDef(BLANK, upgrade_info);
}

void MyView::technical_menu() {
    use_VT(TECH_MENU_1);
    use_VT(TECH_MENU_2);
    _lcd.printDef(TECH_MENU_1, TECH_MENU_2);
}

void MyView::technical_pulse_state(PulseStatus pulse_status) {
    char state[17];
    use_VT(PULSE_STATE);

    switch(pulse_status) {
        case(PulseStatus::NoStatus         ): { strcpy_P(state, VT_PULSE_NO); break; }
        case(PulseStatus::Hypothesis_Type  ): { strcpy_P(state, VT_PULSE_HT); break; }
        case(PulseStatus::Hypothesis_Bisect): { strcpy_P(state, VT_PULSE_HB); break; }
        case(PulseStatus::Stress           ): { strcpy_P(state, VT_PULSE_STRESS); break; }
        case(PulseStatus::Done             ): { strcpy_P(state, VT_PULSE_DONE); break; }
        default: { strcpy_P(state, VT_BLANK); break; }
    }

    _lcd.printDef(PULSE_STATE, state);
}

void MyView::technical_pulse_type(PulseType pulse_type) {
    char type[17];
    use_VT(PULSE_TYPE);

    switch(pulse_type) {
        case(PulseType::NoType  ): { strcpy_P(type, VT_PULSE_TYPE_NO); break; }
        case(PulseType::Resistor): { strcpy_P(type, VT_PULSE_TYPE_R); break; }
        case(PulseType::Both    ): { strcpy_P(type, VT_PULSE_TYPE_B); break; }
        default: { strcpy_P(type, VT_BLANK); break; }
    }

    _lcd.printDef(PULSE_TYPE, type);
}

void MyView::technical_pulse_interval(long interval) {
    use_VT(PULSE_INTER);
    assign_VT(BLANK, pulse_interval);

    const byte STR_BUFFER_LENGTH = 16 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(interval, str_buffer, 10);

    byte offset;
    for (byte i=0; i<16; i++) {
        offset = i;
        if (str_buffer[i] == 0) break;
        pulse_interval[i] = str_buffer[i];
    }

    if (offset + 1 < 16) pulse_interval[offset + 1] = 'm';
    if (offset + 2 < 16) pulse_interval[offset + 2] = 's';

    _lcd.printDef(PULSE_INTER, pulse_interval);
}


// Notification template definitions

void MyView::notify_liberating(const char* team_name, long period) {
    use_VT(LIBERATING);
    use_VT(BY);

    for (byte i=0; i<(14-5); i++) {
        if (team_name[i] == 0) break;
        BY[1 + 5 + i] = team_name[i];
    }

    _notify(LIBERATING, BY, period);
}

void MyView::notify_capturing(const char* team_name, long period) {
    use_VT(CAPTURING);
    use_VT(BY);

    for (byte i=0; i<(14-5); i++) {
        if (team_name[i] == 0) break;
        BY[1 + 5 + i] = team_name[i];
    }

    _notify(CAPTURING, BY, period);
}

void MyView::notify_captured(const char* team_name, long period) {
    use_VT(CAPTURED);
    use_VT(BY);

    for (byte i=0; i<(14-5); i++) {
        if (team_name[i] == 0) break;
        BY[1 + 5 + i] = team_name[i];
    }

    _notify(CAPTURED, BY, period);
}

void MyView::notify_collected(int resource_value, long period) {
    use_VT(COLLECTED);
    assign_VT(BLANK, value_info);

    const byte STR_BUFFER_LENGTH = 14 - 5 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(resource_value, str_buffer, 10);

    for (byte i=0; i<(14-5); i++) {
        if (str_buffer[i] == 0) break;
        value_info[1 + 5 + i] = str_buffer[i];
    }
    value_info[0] = '#';
    value_info[15] = '#';

    _notify(COLLECTED, value_info, period);
}

void MyView::notify_looted(int resource_value, long period) {
    use_VT(LOOTED);
    use_VT(LOOTED_VALUE);

    const byte STR_BUFFER_LENGTH = 7;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(resource_value, str_buffer, 10);

    for (byte i=0; i<(14-11); i++) {
        if (str_buffer[i] == 0) break;
        LOOTED_VALUE[1 + 9 + i] = str_buffer[i];
    }

    _notify(LOOTED, LOOTED_VALUE, period);
}

void MyView::notify_wrong_upgrade_code(long period) {
    use_VT(WRONG_UPGRADE);
    use_VT(WRONG_CODE);
    _notify(WRONG_UPGRADE, WRONG_CODE, period);
}

void MyView::notify_upgrade_already_used(const char* upgrade_name, long period) {
    use_VT(CODE_ALR_USED);
    use_VT(CODE_REW);

    for (byte i=0; i<(14-5); i++) {
        if (upgrade_name[i] == 0) break;
        CODE_REW[1 + 5 + i] = upgrade_name[i];
    }

    _notify(CODE_ALR_USED, CODE_REW, period);
}

void MyView::notify_camp_upgraded(const char* upgrade_name, long period) {
    use_VT(CAMP_UPGRADED);
    use_VT(CODE_REW);

    for (byte i=0; i<(14-5); i++) {
        if (upgrade_name[i] == 0) break;
        CODE_REW[1 + 5 + i] = upgrade_name[i];
    }

    _notify(CAMP_UPGRADED, CODE_REW, period);
}

void MyView::notify_action_cancelled(long period) {
    use_VT(ACTION);
    use_VT(CANCELLED);
    _notify(ACTION, CANCELLED, period);
}

void MyView::notify_eeprom_cleared(long period) {
    use_VT(EEPROM_CLEAR);
    use_VT(PLEASE_RESET);
    _notify(EEPROM_CLEAR, PLEASE_RESET, period);
}

void MyView::notify_owner_saved(const char* owner_name, long period) {
    use_VT(OWNER_SAVE);
    use_VT(SAVED);

    for (byte i=0; i<(14-8); i++) {
        if (owner_name[i] == 0) break;
        OWNER_SAVE[1 + 8 + i] = owner_name[i];
    }

    _notify(OWNER_SAVE, SAVED, period);
}

void MyView::notify_stock_saved(long stock_value, long period) {
    use_VT(STOCK_SAVE);
    use_VT(SAVED);

    const byte STR_BUFFER_LENGTH = 14 - 8 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(stock_value, str_buffer, 10);

    for (byte i=0; i<(14-8); i++) {
        if (str_buffer[i] == 0) break;
        STOCK_SAVE[1 + 8 + i] = str_buffer[i];
    }

    _notify(STOCK_SAVE, SAVED, period);
}

void MyView::notify_loot_saved(long loot_value, long period) {
    use_VT(LOOT_SAVE);
    use_VT(SAVED);

    const byte STR_BUFFER_LENGTH = 14 - 5 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(loot_value, str_buffer, 10);

    for (byte i=0; i<(14-7); i++) {
        if (str_buffer[i] == 0) break;
        LOOT_SAVE[1 + 7 + i] = str_buffer[i];
    }

    _notify(LOOT_SAVE, SAVED, period);
}

void MyView::notify_upgrade_code_set(byte code, const char* code_value, long period) {
    use_VT(UPGRADE_SET);
    assign_VT(BLANK, code_info);

    const byte STR_BUFFER_LENGTH = 3 + 1;
    char str_buffer[STR_BUFFER_LENGTH];
    itoa(code, str_buffer, 10);

    byte offset = 0;
    for (byte i=0; i<(14-9); i++) {
        if (str_buffer[i] == 0) break;
        UPGRADE_SET[1 + 9 + i] = str_buffer[i];
        offset = 1 + 9 + i;
    }

    if (offset + 0 < 14) UPGRADE_SET[1 + offset + 1] = 's';
    if (offset + 1 < 14) UPGRADE_SET[1 + offset + 2] = 'e';
    if (offset + 2 < 14) UPGRADE_SET[1 + offset + 3] = 't';

    for (byte i=0; i<14; i++) {
        if (code_value[i] == 0) break;
        code_info[1 + i] = code_value[i];
    }
    code_info[0] = '#';
    code_info[15] = '#';

    _notify(UPGRADE_SET, code_info, period);
}

void MyView::notify_default_view(long period) {
    use_VT(VIEW);
    use_VT(VIEW_DEFAULT);
    _notify(VIEW, VIEW_DEFAULT, period);
}

void MyView::notify_setup_view(long period) {
    use_VT(VIEW);
    use_VT(VIEW_SETUP);
    _notify(VIEW, VIEW_SETUP, period);
}

void MyView::notify_fixes_view(long period) {
    use_VT(VIEW);
    use_VT(VIEW_FIX);
    _notify(VIEW, VIEW_FIX, period);
}

void MyView::notify_technical_view(long period) {
    use_VT(VIEW);
    use_VT(VIEW_TECH);
    _notify(VIEW, VIEW_TECH, period);
}
