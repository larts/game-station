#include <Wire.h>

#include <myConfig.h>
#include <myHelpers.h>
#include <myLCD.h>
#include <myView.h>
#include <myLED.h>
#include <myInput.h>
#include <myFrame.h>
#include <myTeams.h>
#include <mySpinner.h>
#include <myPulse.h>
#include <myEEPROM.h>

#include "ThroneCamp.hpp"

/**
 * This year, game starts at 10am.
 */
const long GAME_STARTS_AT = (long)10 * 60 * 60 * 1000; // h*m*s*ms
const long NOTIFICATION_PERIOD = 3000;

static MyLED led = MyLED();
static MyLCD lcd = MyLCD();
static MyInput input = MyInput();
static MyFrame frame = MyFrame(20);
static MyPulse pulse = MyPulse(lcd);
static MyView view = MyView(lcd, frame, NOTIFICATION_PERIOD);
static MyEEPROM eeprom;

static ThroneCamp camp = ThroneCamp(frame);

const unsigned int HEARTBEAT_INTERVAL = 60000;
const unsigned int LED_ALTERNATE_INTERVAL = 500;

static MY_PINS activity;
static MY_PINS activity_team;
static int activity_crc;
static long activity_alarm;
static bool input_active = false;
const byte MAX_INPUT_CODE_LENGTH = 16;
static char input_code[MAX_INPUT_CODE_LENGTH + 1] = {0};
static byte input_code_length = 0;

static long tmpActionVariable = -1;

static long awake_alarm = INT32_MIN;
const int awake_PERIOD = 10000;

enum class CurrentView : unsigned char {
    Default = 0,
    Setup = 1,
    Quick_Fixes = 2,
    Technical = 3,
};
static CurrentView current_view = CurrentView::Default;

extern unsigned int __bss_end;
extern unsigned int __heap_start;
extern void *__brkval;

// Returns free memory in bytes, useful for debugging
uint16_t getFreeSram() {
    uint8_t newVariable;

    if ((uint16_t)__brkval == 0) {
        return ((uint16_t)&newVariable) - ((uint16_t)&__bss_end);
    } else {
        return ((uint16_t)&newVariable) - ((uint16_t)__brkval);
    }
}

static void attempt_error(ThroneCampStatus ThroneCampStatusCode) {
    view.attempt_error(ThroneCampStatusCode);
    activity = (MY_PINS)0;
}
static void attempt_capture(bool liberate = false) {
    activity_team = liberate ? getTeamByCRC(input.keysPress.CRC_Team ^ camp.getOwner().crc).pin : activity_team;
    MY_PINS team = liberate ? team_ADMIN.pin : activity_team;
    Response resp = camp.startCapture(getTeam(team), true);

    if(resp.status != ThroneCampStatus::OK) {
        return attempt_error(resp.status);
    }

    long remaining_time = activity_alarm - frame.clocktime_ms;

    if (liberate) {
        view.liberation_cooldown_bar(remaining_time, ACTIVITY_PERIOD_1);
    } else {
        view.capture_cooldown_bar(remaining_time, ACTIVITY_PERIOD_1);
    }

    if(remaining_time < 0) {
        camp.startCapture(getTeam(activity_team));

        if (camp.isCapturing()) {
            led.setAlternateTeamColors(camp.getOwner(), getTeam(activity_team));
        } else {
            led.setTeamColor(camp.getOwner());
        }

        if (liberate) {
            view.notify_liberating(getTeam(activity_team).name);
        } else {
            view.notify_capturing(getTeam(activity_team).name);
        }

        activity = (MY_PINS)0;
        return;
    }
}
static void finish_capture() {
    camp.finishCapture();
    led.setTeamColor(camp.getOwner());
    view.notify_captured(camp.getOwner().name);
    return;
}
static void attempt_collect() {
    Response resp = camp.collect(getTeam(activity_team), true);

    if(resp.status != ThroneCampStatus::OK) {
        return attempt_error(resp.status);
    }

    long remaining_time = activity_alarm - frame.clocktime_ms;
    byte estimated_reward = camp.predictStockpile(activity_alarm);
    view.collect_cooldown_bar(remaining_time, ACTIVITY_PERIOD_2, estimated_reward, camp.getResource());

    if(remaining_time < 0) {
        Response resp = camp.collect(getTeam(activity_team));

        view.notify_collected(resp.value, NOTIFICATION_PERIOD * 2);
        activity = (MY_PINS)0;
        return;
    }
}
static void attempt_loot() {
    Response resp = camp.loot(getTeam(activity_team), true);

    if(resp.status != ThroneCampStatus::OK) {
        return attempt_error(resp.status);
    }

    long remaining_time = activity_alarm - frame.clocktime_ms;
    byte estimated_reward = min(camp.predictStockpile(activity_alarm), 1);
    view.loot_cooldown_bar(remaining_time, ACTIVITY_PERIOD_3, estimated_reward, camp.getResource());

    if(remaining_time < 0) {
        camp.loot(getTeam(activity_team));
        camp.cancleCapture();
        view.notify_looted(resp.value);
        activity = (MY_PINS)0;
        return;
    }
}
static void attempt_upgrade() {
    Response resp = camp.upgrade(getTeam(activity_team), "", true);
    if(resp.status != ThroneCampStatus::OK) {
        return attempt_error(resp.status);
    }

    long remaining_time = activity_alarm - frame.clocktime_ms;
    view.upgrade_cooldown_bar(remaining_time, ACTIVITY_PERIOD_4);

    if(remaining_time < 0) {
        input_active = true;
        activity = (MY_PINS)0;
    }
}
static void append_input_code(char ch) {
    if (input_code_length < MAX_INPUT_CODE_LENGTH) {
        input_code[input_code_length] = ch;
        input_code[input_code_length + 1] = 0;
        input_code_length++;
    }
}
static void input_upgrade_code() {
    view.upgrade_code_input(input_code);

    switch(input.keysDown.Team) {
            case(MY_PINS::Team_Purple): { append_input_code('A'); break; }
            case(MY_PINS::Team_Blue  ): { append_input_code('E'); break; }
            case(MY_PINS::Team_Green ): { append_input_code('H'); break; }
            case(MY_PINS::Team_Red   ): { append_input_code('M'); break; }
            case(MY_PINS::Team_Orange): { append_input_code('O'); break; }
            case(MY_PINS::Team_Gold  ): { append_input_code('S'); break; }
            case(MY_PINS::Team_Pink  ): { append_input_code('T'); break; }
            default: break;
    }
}
static void finish_upgrade() {
    Response resp = camp.upgrade(getTeam(activity_team), input_code);
    activity = (MY_PINS)0;
    input_code[0] = 0;
    input_code_length = 0;
    input_active = false;
    if (resp.status == ThroneCampStatus::WrongUpgradeCode){
        view.notify_wrong_upgrade_code();
        return;
    }
    if (resp.status == ThroneCampStatus::AlreadyUpgraded){
        view.notify_upgrade_already_used(resp.text);
        return;
    }
    view.notify_camp_upgraded(resp.text, 3*NOTIFICATION_PERIOD);
}
static void view_default() {
    if (frame.clocktime_ms < 0) {
        view.game_not_started(-frame.clocktime_ms);
        return;
    }

    if (frame.clocktime_ms > 0 && frame.clocktime_ms - frame.prev_frame_ms < 0){
        camp.setStockpile(team_ADMIN, INITIAL_RESOURCES_AT_GAME_START);
        camp.setStartingCollectCooldown(team_ADMIN);
    }

    if (camp.getProduceIn() > 0) {
        view.camp_is_looted(camp.getProduceIn());
        return;
    }

    if (input_active && input.keysPress.Action == MY_PINS::Action_4)
    {
        return input_upgrade_code();
    }

    // If activity is ongoing, shortcut to it if buttons are still being held.
    if((bool)activity) {
        if(activity_crc != input.keysPress.CRC) {
            view.notify_action_cancelled();
            activity = (MY_PINS)0;
            return;
        };

        switch(activity) {
            case(MY_PINS::Action_1): return attempt_capture();//input.keysPress.CRC_Team & camp.getOwner().crc && input.keysPress.CRC_Team ^ camp.getOwner().crc);
            case(MY_PINS::Action_2): return attempt_collect();
            case(MY_PINS::Action_3): return attempt_loot();
            case(MY_PINS::Action_4): return attempt_upgrade();
            default: break;
        }
    }

    if(input.keysPress.CRC == 0) {

        // Action that happens when action button is released
        if((bool)input.keysUp.Action_4 && input_active){
            return finish_upgrade();
        }

        // Default print out
        long remaining_time = camp.getCaptureIn() - frame.clocktime_ms;
        if(camp.isCapturing() && remaining_time > 0 ){
            if (camp.isCapturing()) {
                view.capture_progress_bar(remaining_time, ACTIVITY_PERIOD_5);
            } else {
                //view.liberation_progress_bar(remaining_time, ACTIVITY_PERIOD_5);
            }
        }
        else if (camp.isCapturing() && remaining_time <= 0 ) {
            finish_capture();
        }
        else {
            view.idle(
                camp.getName(),
                frame.clocktime_ms + GAME_STARTS_AT,
                camp.getCollectIn()
            );
        }
        return;
    }

    // If both Team and Actions is presed, initiate the action.
    if((bool)input.keysPress.Action && (bool)input.keysPress.Team && input.keysPress.Team != team_pink.pin) {
        activity = input.keysPress.Action;
        activity_team = input.keysPress.Team;
        activity_crc = input.keysPress.CRC;
        switch(input.keysPress.Action) {
            case(MY_PINS::Action_1):
                activity_alarm = frame.clocktime_ms + ACTIVITY_PERIOD_1;
                return attempt_capture();//input.keysPress.CRC_Team & camp.getOwner().crc && input.keysPress.CRC_Team ^ camp.getOwner().crc);
            case(MY_PINS::Action_2):
                activity_alarm = frame.clocktime_ms + ACTIVITY_PERIOD_2;
                return attempt_collect();
            case(MY_PINS::Action_3):
                activity_alarm = frame.clocktime_ms + ACTIVITY_PERIOD_3;
                return attempt_loot();
            case(MY_PINS::Action_4):
                activity_alarm = frame.clocktime_ms + ACTIVITY_PERIOD_4;
                return attempt_upgrade();
            default:
                break;
        }
    }

    // TODO: Show something useful when only Team is pressed.
    if((bool)input.keysPress.Team) {
        view.team_selected(getTeam(input.keysPress.Team).name);
    }

    const char* capturer_name = camp.isCapturing() ? camp.getCapturer().name : nullptr;

    // If only Action is pressed, show action info.
    if((bool)input.keysPress.Action) {
        switch(input.keysPress.Action) {
            case(MY_PINS::Action_1):
                view.capture_status(camp.getOwner().name, capturer_name);
                return;
            case(MY_PINS::Action_2): {
                view.collect_status(
                    camp.getStockpile(),
                    camp.getResource()
                );
                return;
            }
            case(MY_PINS::Action_3):
                view.loot_status();
                return;
            case(MY_PINS::Action_4):
                view.upgrade_status(
                    camp.getUpgradeCount(),
                    1.0 / camp.getProductionSpeed()
                );
                return;
            default:
                break;
        }
    }
}
static void view_setup() {
    switch(input.keysPress.Action) {
        default: {
            view.setup_menu();
            break;
        }
        case(MY_PINS::Action_2): {
            view.setup_uptime(frame.clocktime_ms, frame.getTurnedOn());
            const long MAX_TURNED_ON = 8 * 60 * 60000;
            const long MIN_TURNED_ON = -3 * 60 * 60000;
            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {frame.setTurnedOn(MAX_TURNED_ON); break; }
                case(MY_PINS::Team_Blue  ): {frame.setTurnedOn(min(MAX_TURNED_ON, frame.getTurnedOn() + (long)60 * 60 * 1000)); break; }
                case(MY_PINS::Team_Green ): {frame.setTurnedOn(min(MAX_TURNED_ON, frame.getTurnedOn() + (long)60 * 1000)); break; }
                case(MY_PINS::Team_Red   ): {frame.setTurnedOn(max(MIN_TURNED_ON, frame.getTurnedOn() - (long)60 * 1000)); break; }
                case(MY_PINS::Team_Orange): {frame.setTurnedOn(max(MIN_TURNED_ON, frame.getTurnedOn() - (long)60 * 60 * 1000)); break; }
                case(MY_PINS::Team_Gold  ): {frame.setTurnedOn(MIN_TURNED_ON); break; }
                default: break;
            }
            return;
        }
        case(MY_PINS::Action_3): {
            view.setup_type(
                camp.getId(),
                camp.getResource(),
                camp.getName()
            );
            const unsigned char MAX_ID = 27;
            const unsigned char MIN_ID = 1;
            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {camp.setId(MAX_ID); break; }
                case(MY_PINS::Team_Blue  ): {camp.setId(min(MAX_ID, (unsigned char)(camp.getId() + 1))); break; }
                case(MY_PINS::Team_Green ): {camp.setId(max(MIN_ID, (unsigned char)(camp.getId() - 1))); break; }
                case(MY_PINS::Team_Red   ): {camp.setId(MIN_ID); break; }
                default: break;
            }
            return;
        }
        case(MY_PINS::Action_4): {
            if(input.keysDown.Action_4) {
                activity_alarm = frame.clocktime_ms + 5000;
            }

            long remaining_time = activity_alarm - frame.clocktime_ms;
            view.clear_eeprom_safety_bar(remaining_time, 5000);

            if(remaining_time < 0) {
                eeprom.clean();
                view.notify_eeprom_cleared();
                activity = (MY_PINS)0;
                return;
            }

        }
    }
}
static void view_quick_fixes() {
    switch(input.keysPress.Action) {
        default: {
            view.fix_menu();
            break;
        }
        case(MY_PINS::Action_1): {
            Team owner = tmpActionVariable == -1 ? camp.getOwner() : getTeam((MY_PINS)tmpActionVariable);
            view.fix_capture(owner.name);
            if((bool)input.keysDown.Team) {
                tmpActionVariable = (long)input.keysDown.Team;
            }
            return;
        }
        case(MY_PINS::Action_2): {
            float stock = tmpActionVariable == -1 ? camp.getStockpile() : tmpActionVariable;
            view.fix_stock(stock, camp.getResource());
            const float MAX_STOCK = 100.0;
            const float MIN_STOCK = -50.0;
            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {tmpActionVariable = MAX_STOCK; break; }
                case(MY_PINS::Team_Blue  ): {tmpActionVariable = min(MAX_STOCK, stock + (long)10); break; }
                case(MY_PINS::Team_Green ): {tmpActionVariable = min(MAX_STOCK, stock + (long)1); break; }
                case(MY_PINS::Team_Red   ): {tmpActionVariable = max(MIN_STOCK, stock - (long)1); break; }
                case(MY_PINS::Team_Orange): {tmpActionVariable = max(MIN_STOCK, stock - (long)10); break; }
                case(MY_PINS::Team_Gold  ): {tmpActionVariable = MIN_STOCK; break; }
                default: break;
            }
            return;
        }
        case(MY_PINS::Action_3): {
            view.fix_loot(camp.getLootIn(), camp.getProduceIn());
            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {camp.setLooted(team_ADMIN, PRODUCTION_AFTER_LOOT_INTERVAL, READY_AFTER_LOOT_INTERVAL); break; }
                case(MY_PINS::Team_Blue  ): {camp.setLooted(team_ADMIN, 0, 0); break; }
                default: break;
            }
            return;
        }
        case(MY_PINS::Action_4): {
            view.upgrade_status(
                camp.getUpgradeCount(),
                1.0 / camp.getProductionSpeed()
            );
            char upgrades = tmpActionVariable == -1 ? camp.getUpgrades() : camp.getUpgrades() ^ (0b0001<<tmpActionVariable);
            view.fix_upgrade(upgrades);

            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {tmpActionVariable = 0; break; }
                case(MY_PINS::Team_Blue  ): {tmpActionVariable = 1; break; }
                case(MY_PINS::Team_Green ): {tmpActionVariable = 2; break; }
                case(MY_PINS::Team_Red   ): {tmpActionVariable = 3; break; }
                default: break;
            }
            return;
        }
    }

    if((bool)input.keysUp.Action) {
        if(tmpActionVariable == -1) {
            return;
        }

        switch(input.keysUp.Action) {
            case(MY_PINS::Action_1): {
                if(tmpActionVariable != (long)camp.getOwner().pin) {
                    const Team team = getTeam((MY_PINS)tmpActionVariable);
                    camp.capture(team);
                    led.setTeamColor(camp.getOwner());
                    view.notify_owner_saved(team.name);
                }
                break;
            }
            case(MY_PINS::Action_2): {
                if(tmpActionVariable != (long)camp.getStockpile()) {
                    camp.setStockpile(team_ADMIN, tmpActionVariable);
                    view.notify_stock_saved(tmpActionVariable);
                }
                break;
            }
            case(MY_PINS::Action_3): {
                if(tmpActionVariable != -1) {
                    camp.loot(team_ADMIN, tmpActionVariable);
                    view.notify_loot_saved(tmpActionVariable);
                }
                break;
            }
            case(MY_PINS::Action_4): {
                if(tmpActionVariable != -1) {
                    camp.setUpgrades(team_ADMIN, tmpActionVariable);

                    char debug_upgrade_code[17];
                    camp.getUpgradeCode(tmpActionVariable, debug_upgrade_code, 17);

                    view.notify_upgrade_code_set(tmpActionVariable+1, debug_upgrade_code);
                }
                break;
            }
            default:
                break;
        }

        tmpActionVariable = -1;
    }

}
static void view_technical() {
    switch(input.keysPress.Action) {
        default: {
            view.technical_menu();
            break;
        }
        case(MY_PINS::Action_1): {
            view.technical_pulse_state(pulse.getStatus());

            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {pulse.setStatus(PulseStatus::NoStatus); break; }
                case(MY_PINS::Team_Blue  ): {pulse.setStatus(PulseStatus::Hypothesis_Type); break; }
                case(MY_PINS::Team_Green ): {pulse.setStatus(PulseStatus::Hypothesis_Bisect); break; }
                case(MY_PINS::Team_Red   ): {pulse.setStatus(PulseStatus::Stress); break; }
                case(MY_PINS::Team_Orange): {pulse.setStatus(PulseStatus::Done); break; }
                default: break;
            }
            return;
        }
        case(MY_PINS::Action_2): {
            view.technical_pulse_type(pulse.getType());

            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {pulse.setType(PulseType::NoType); break; }
                case(MY_PINS::Team_Blue  ): {pulse.setType(PulseType::Resistor); break; }
                case(MY_PINS::Team_Green ): {pulse.setType(PulseType::Both); break; }
                default: break;
            }
            return;
        }
        case(MY_PINS::Action_3): {
            view.technical_pulse_interval(pulse.getInterval());
            switch(input.keysDown.Team) {
                case(MY_PINS::Team_Purple): {pulse.setInterval(5000); break; }
                case(MY_PINS::Team_Blue  ): {pulse.setInterval(min((long)5000, pulse.getInterval() + (long)1000)); break; }
                case(MY_PINS::Team_Green ): {pulse.setInterval(min((long)5000, pulse.getInterval() + (long)100)); break; }
                case(MY_PINS::Team_Red   ): {pulse.setInterval(max((long)100, pulse.getInterval() - (long)100)); break; }
                case(MY_PINS::Team_Orange): {pulse.setInterval(max((long)100, pulse.getInterval() - (long)1000)); break; }
                case(MY_PINS::Team_Gold  ): {pulse.setInterval(100); break; }
                default: break;
            }
            return;
        }
    }
}
void setup() {
    #ifdef USE_SERIAL
    Serial.begin(9600);
    Serial.println(F("Serial console started"));
    #endif

    lcd.init(); // initialize the lcd
    lcd.backlight(); //turn on lcd baclight
    camp.setup();

    if (camp.isCapturing()) {
        led.setAlternateTeamColors(camp.getOwner(), camp.getCapturer());
    } else {
        led.setTeamColor(camp.getOwner());
    }
}
void loop() {
    // Every frame.
    lcd.flush();
    frame.next();
    input.read();
    lcd.clear();

    // Every interval.
    if(frame.oncePerInterval(pulse.getInterval())) {
        pulse.drain();
    }
    if(frame.oncePerInterval(HEARTBEAT_INTERVAL)) {
        frame.saveHeartbeat();
    }
    if(frame.oncePerInterval(LED_ALTERNATE_INTERVAL)) {
        led.alternateColors();
    }

    if(input.keysPress.CRC) {
        awake_alarm = frame.clocktime_ms + awake_PERIOD;
    }
    if(awake_alarm > frame.clocktime_ms) {
        lcd.backlight();
    } else {
        lcd.noBacklight();
    }

    // Change view, if needed. Press all 4 actions buttons + 1 team button.
    if(input.keysPress.CRC_Action == 0b1111) {
        tmpActionVariable = -1;
        switch(input.keysPress.Team) {
            case(MY_PINS::Team_Purple): {
                current_view = CurrentView::Default;
                view.notify_default_view();
                break;
            }
            case(MY_PINS::Team_Blue): {
                current_view = CurrentView::Setup;
                view.notify_setup_view();
                break;
            }
            case(MY_PINS::Team_Green): {
                current_view = CurrentView::Quick_Fixes;
                view.notify_fixes_view();
                break;
            }
            case(MY_PINS::Team_Red): {
                current_view = CurrentView::Technical;
                view.notify_technical_view();
                break;
            }
            default: break;
        }
    }

    // If notification has not yet expired, show it.
    if (view.has_active_notification()) {
        view.render_notification();
        return;
    } else {
        view.reset_notification_alarm();
    }

    // Proceed as per the active view.
    switch(current_view) {
        case(CurrentView::Default): view_default(); break;
        case(CurrentView::Setup): view_setup(); break;
        case(CurrentView::Quick_Fixes): view_quick_fixes(); break;
        case(CurrentView::Technical): view_technical(); break;
        default: break;
    }

}
