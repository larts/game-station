#ifndef MODULE_MY_EEPROM
#define MODULE_MY_EEPROM

#include "myConfig.h"
#include <Arduino.h>
#include <EEPROM.h>

class MyEEPROM {
public:
    void clean() const;

    void putHeartbeat(short beat) const;
    short getHeartbeat() const;

    void putPile(short pile) const;
    short getPile() const;

    void putId(unsigned char type) const;
    unsigned char getId() const;

    void putPulse(Pulse pulse) const;
    Pulse getPulse() const;

    void appendLog(LogEntry log) const;
    LogEntry getLog(short index) const;
    short getLogCount() const;
};

#endif