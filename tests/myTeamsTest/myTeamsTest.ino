#line 2 "myTeamsTest"

#include <Arduino.h>
#include <AUnit.h>
#include "myTeams.h"

using aunit::TestRunner;

test(myTeamsTest, getTeam) {
    assertEqual(&getTeam(MY_PINS::Team_Purple), &team_purple);
    assertEqual(&getTeam(MY_PINS::Team_Blue), &team_blue);
    assertEqual(&getTeam(MY_PINS::Team_Green), &team_green);
    assertEqual(&getTeam(MY_PINS::Team_Red), &team_red);
    assertEqual(&getTeam(MY_PINS::Team_Orange), &team_orange);
    assertEqual(&getTeam(MY_PINS::Team_Gold), &team_gold);
    assertEqual(&getTeam(MY_PINS::team_ADMIN), &team_ADMIN);
}

test(myTeamsTest, getTeamByCRC) {
    assertEqual(&getTeamByCRC(team_purple.crc), &team_purple);
    assertEqual(&getTeamByCRC(team_blue.crc), &team_blue);
    assertEqual(&getTeamByCRC(team_green.crc), &team_green);
    assertEqual(&getTeamByCRC(team_red.crc), &team_red);
    assertEqual(&getTeamByCRC(team_orange.crc), &team_orange);
    assertEqual(&getTeamByCRC(team_gold.crc), &team_gold);
    assertEqual(&getTeamByCRC(team_ADMIN.crc), &team_ADMIN);
}

//---------------------------------------------------------------------------

void setup() {
    Serial.begin(9600);
}

void loop() {
    TestRunner::run();
}
