#include <Arduino.h>

class LiquidCrystal_I2C {
    private:
        char screen[50][50] = {
            ' '
        };
    int cursorX = 0;
    int cursorY = 0;
    int width = 0;
    int height = 0;
    int isBacklightOn = 1;

    public:
        LiquidCrystal_I2C(uint8_t magic, uint8_t w, uint8_t h) {
            width = w;
            height = h;
        }

    void init() {}
    void clear() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                screen[i][j] = ' ';
            }
        }
    }
    String serizalizeScreen() {
        String output = "";
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                output = output + screen[i][j];
            }
            output = output + '\n';
        }
        return output;
    }
    String serizalizeScreenFramed() {
        String frame = "+";
        for (int i = 0; i < width; i++) frame = frame + "-";
        frame = frame + "+\n";

        String rawOutput = serizalizeScreen();
        String output = "|";
        for (unsigned int i = 0; i < rawOutput.length() - 1; i++) {
            if (rawOutput[i] == '\n') {
                output = output + "|\n|";
            } else {
                output = output + rawOutput[i];
            }
        }
        output = output + "|\n";

        return frame + output + frame;
    }
    void backlight() {
        isBacklightOn = 1;
    }
    void noBacklight() {
        isBacklightOn = 0;
    }
    void setCursor(int x, int y) {
        cursorX = x;
        cursorY = y;
    }
    void print(const uint8_t number) {
        auto numberAsText = String(number);
        print(numberAsText);
    };
    void print(const uint8_t text, int bin) {
        screen[cursorX][cursorY] = text;
        cursorX++;
    };
    void print(const String text) {
        for (unsigned int i = 0; i < text.length(); i++) {
            screen[cursorX][cursorY] = text[i];
            cursorX++;
        }
    }
};
