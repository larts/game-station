#include <myConfig.h>
#include <myLCD.h>
#include <myLED.h>
#include <myInput.h>
#include <myFrame.h>
#include <myTeams.h>


const String TN0 = "ZERO";
const String TNP = "PURPLE";
const String TNW = "PINK";
const String TNY = "GOLD";
const String TNO = "ORANGE";
const String TNR = "RED";
const String TNG = "GREEN";
const String TNB = "BLUE";
const String ACT1N = "1. Capturing";
const String ACT2N = "2. Collecting";
const String ACT3N = "3. Looting";
const String ACT4N = "4. Upgrading";

static MyLCD lcd = MyLCD();
const MyLED led = MyLED();
static MyInput input = MyInput();
static MyFrame frame = MyFrame(20);
static MyEEPROM eeprom;
unsigned long Looted[8] = {0, 0, 0, 0, 0, 0, 0, 0};

static MY_PINS owner = MY_PINS::Team_Purple;


static void owner_led() { //ieslēdz attiecīgās komandas krāsas led kombināciju
    switch (owner) {
        case MY_PINS::Team_Purple:
            led.setTeamColor(team_purple);
            break;
        case MY_PINS::Team_Blue:
            led.setTeamColor(team_blue);
            break;
        case MY_PINS::Team_Green:
            led.setTeamColor(team_green);
            break;
        case MY_PINS::Team_Red:
            led.setTeamColor(team_red);
            break;
        case MY_PINS::Team_Orange:
            led.setTeamColor(team_orange);
            break;
        case MY_PINS::Team_Gold:
            led.setTeamColor(team_gold);
            break;
        case MY_PINS::Team_Pink:
            led.setTeamColor(team_pink);
            break;
        default:
            led.setRGB(0, 0, 0);
            break;
    }
}

void setup() {
    lcd.init(); // initialize the lcd
    lcd.backlight(); //turn on lcd baclight

    lcd.printAt(0, 0, "= Validate =====");
    lcd.printAt(0, 1, "= Hardware =====");
    lcd.flush();
    delay(1000);

    lcd.printAt(0, 1, "Press any button");

    short count = eeprom.getLogCount();
    for (short i = 0; i < count; i++) {
        LogEntry log = eeprom.getLog(i);
        if(log.heartbeat <= 7 * 30 * 60){
            continue;
        }
        //Get captures totals
        /*if(log.type == EventType::Capture){
            Captured[(unsigned char)team - 1] += (log.heartbeat - lastCapture);
            team = log.team;
            lastCapture = log.heartbeat;
        }
        //Get collect totals
        if(log.type == EventType::Collect){
            Collected[(unsigned char)log.team - 1] += log.value;
        }*/
        //Get loot totals
        if(log.type == EventType::Loot){
            Looted[(unsigned char)log.team - 1] += 1;
        }
        //Get upgrade totals
        /*if(log.type == EventType::Upgrade){
            Upgraded[(unsigned char)log.team - 1] = Upgraded[(unsigned char)log.team - 1] ^ (0b0001<<log.value);
        }*/
    }
}

void loop() {
    lcd.flush();
    frame.next();
    input.read();

    if (input.keysPress.Action_1) {
        lcd.printAt(0, 0, ACT1N);
        if (input.keysPress.Team_Purple){owner=MY_PINS::Team_Purple;}
        else if (input.keysPress.Team_Blue){owner=MY_PINS::Team_Blue;}
        else if (input.keysPress.Team_Green){owner=MY_PINS::Team_Green;}
        else if (input.keysPress.Team_Red){owner=MY_PINS::Team_Red;}
        else if (input.keysPress.Team_Orange){owner=MY_PINS::Team_Orange;}
        else if (input.keysPress.Team_Gold){owner=MY_PINS::Team_Gold;}
        else if (input.keysPress.Team_Pink){owner=MY_PINS::Team_Pink;}
    }
    else if (input.keysPress.Action_2){lcd.printAt(0, 0, ACT2N);}
    else if (input.keysPress.Action_3){lcd.printAt(0, 0, ACT3N);}
    else if (input.keysPress.Action_4){lcd.printAt(0, 0, ACT4N);}
    else if (input.keysPress.Team_Purple){lcd.printAt(0, 0, team_purple.name+": "+String(Looted[7]));}
    else if (input.keysPress.Team_Blue){lcd.printAt(0, 0, team_blue.name+": "+String(Looted[6]));}
    else if (input.keysPress.Team_Green){lcd.printAt(0, 0, team_green.name+": "+String(Looted[5]));}
    else if (input.keysPress.Team_Red){lcd.printAt(0, 0, team_red.name+": "+String(Looted[4]));}
    else if (input.keysPress.Team_Orange){lcd.printAt(0, 0, team_orange.name+": "+String(Looted[3]));}
    else if (input.keysPress.Team_Gold){lcd.printAt(0, 0, team_gold.name+": "+String(Looted[2]));}
    else if (input.keysPress.Team_Pink){lcd.printAt(0, 0, team_pink.name+": "+String(Looted[1]));}
    else {lcd.printAt(0, 0, "                ");}

    owner_led();
}
