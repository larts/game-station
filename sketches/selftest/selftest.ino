#include <myLCD.h>
#include <myConfig.h>
#include <myInput.h>
#include <myFrame.h>
#include <myPulse.h>
#include <myEEPROM.h>

static MyLCD lcd = MyLCD();
static MyInput input = MyInput();
static MyFrame frame = MyFrame(20);
static MyPulse pulse = MyPulse(lcd);
static MyEEPROM eeprom = MyEEPROM();


void setup() {
    lcd.init();
    lcd.backlight();

    lcd.printAt(0, 0, "= Self-Test ====");
    lcd.printAt(0, 1, "================");
    lcd.flush();
    delay(1000);

    lcd.clear();
    lcd.printAt(0, 0, "Init|         | ");
    lcd.noBacklight();
    digitalWrite((uint8_t)MY_PINS::Power_Sink, LOW); // sets the digital pin 13 on
    digitalWrite(13, LOW); // sets the digital pin 13 on
}

/**
 * PULSE_STATUS := [null, "hypothesis_type", "hypothesis_interval", "confirmed"]
 *
 * Ask to reset.
 *
 * advance type; if hypothesis_interval skip
 * 1. PulseType := ["none", "RES", "both"].
 * 1.1. Try to survive without anything. If YES - go with nothing.
 * 1.2. Try to survive with RES only. if YES - bisect with RES.
 * 1.3. Try to survive with both. if YES - bisect with both.
 * 1.4. Declare impossible.
 *
 * advance bisect; if confirmed skip
 * 2. PULSE_INTERVAL := pow(2,7) of 100 ms (0 to 12700 ms)
 * 2.1. bisect until max precision
 *
 * stress test: just lower interval.
 * 3. Stress test for 3 cycles
 * 3.1. ask to skip, default to just lower.
 * 3.2. just lower it.
 **/

const long PULSE_ALARM_PERIOD = 12000;  // it should die within 10 seconds.
static long last_alarm = INT16_MIN;
static char progress = 0;

void loop() {
    lcd.flush();
    frame.next();
    input.read();
    if(frame.oncePerInterval(pulse.getInterval(), last_alarm)) {
        pulse.drain();
        lcd.printAt(15, 0, "!");
    } else {
        lcd.printAt(15, 0, ".");
    }

    // If any keypress, RESET IT.
    if(input.keysPress.CRC > 0) {
        digitalWrite((uint8_t)MY_PINS::Power_Sink, HIGH);
        lcd.backlight();
        lcd.printAt(0, 0, "Resetting!      ");
        progress=0;
        last_alarm=INT16_MIN;
        pulse.setStatus(NoStatus);
        pulse.setType(NoType);
        pulse.setInterval(0);
        lcd.printAt(0, 1, "S:" + String(pulse.getStatus()) + " T:" + String(pulse.getType()) + " I:" + String(pulse.getInterval()) + "   ");
        lcd.flush();
        delay(2000);
        lcd.printAt(0, 0, "Init|         | ");
        digitalWrite((uint8_t)MY_PINS::Power_Sink, LOW);
        lcd.noBacklight();
    }

    if(pulse.getStatus() == NoStatus && last_alarm + PULSE_ALARM_PERIOD < frame.uptime_ms) {  // Just resetted.

        lcd.printAt(0, 1, "Try NO pulse.. ");
        pulse.setStatus(Hypothesis_Type);
        pulse.setType(NoType);
        last_alarm = frame.uptime_ms;

    }

    if(pulse.getStatus() == Hypothesis_Type && last_alarm + PULSE_ALARM_PERIOD < frame.uptime_ms) {

        if(last_alarm == INT16_MIN) {  // I'm here because I died. Increase the type and try again!
            if(pulse.getType() == Both) {
                lcd.printAt(0, 1, "Impossible!");
                lcd.flush();
                delay(60000);
            }
            pulse.setType((PulseType)(pulse.getType() + 1));
            last_alarm = frame.uptime_ms;
            lcd.printAt(0, 1, "Try T" + String(pulse.getType()) + " pulse.. ");
            progress = (char)(pulse.getType());
        } else {  // I'm here because I survived. Proceed!
            pulse.setStatus(Hypothesis_Bisect);
            pulse.setInterval(0);
        }

    }

    if(pulse.getStatus() == Hypothesis_Bisect && last_alarm + PULSE_ALARM_PERIOD < frame.uptime_ms) {

        int step = 0;
        if(pulse.getInterval() / 100 == 0)           {step = 6400; progress=2;}  // Initial state.
        else if(pulse.getInterval() / 100 % 64 == 0) {step = 3200; progress=3;}
        else if(pulse.getInterval() / 100 % 32 == 0) {step = 1600; progress=4;}
        else if(pulse.getInterval() / 100 % 16 == 0) {step =  800; progress=5;}
        else if(pulse.getInterval() / 100 % 8 == 0)  {step =  400; progress=6;}
        else if(pulse.getInterval() / 100 % 4 == 0)  {step =  200; progress=7;}
        else {step = 100; progress=8;}

        if(last_alarm == INT16_MIN) {  // I'm here because I died. Bisect down!
            pulse.setInterval(pulse.getInterval() - step);
        } else {  // I'm here because I survived. Bisect up!
            pulse.setInterval(pulse.getInterval() + step);
        }

        if(step > 100) {  // Iterate the bisect!
            lcd.printAt(0, 1, String(pulse.getType()) + "B(" + String(step) + "):         ");
            lcd.printAt(10, 1, String(pulse.getInterval()));
            last_alarm = frame.uptime_ms;
        } else {  // I'm done bisecting, proceed!
            pulse.setStatus(Stress);
            if(pulse.getType() == NoType){
                pulse.setStatus(Done);
                pulse.setType(Resistor);
                pulse.setInterval(3000);  // To be on safe side (blinking dot uses energy too!).
            }
        }
    }

    if(pulse.getStatus() == Stress && last_alarm + PULSE_ALARM_PERIOD * 3 < frame.uptime_ms) {

        if(last_alarm == INT16_MIN) {  // I'm here because I died. Lower the interval, try again!
            pulse.setInterval(pulse.getInterval() - 100);
            last_alarm = frame.uptime_ms;
            progress = 8;
        } else {  // I'm here because I survived. Proceed!
            pulse.setStatus(Done);
            pulse.setInterval(pulse.getInterval() - 1000);  // To be on safe side (blinking dot uses energy too!).
        }

        lcd.printAt(0, 1, "Test I:" + String(pulse.getInterval()) + "ms..");
    }

    if(pulse.getStatus() == Done) {

        lcd.printAt(0, 0, "Init done!      ");
        lcd.printAt(0, 1, "T:" + String(pulse.getType()) + "  I:" + String(pulse.getInterval()) + "ms    ");

    } else {

        // "Init|##>.......|"
        for(char i=0; i<16-7; i++) {
            if(i < progress) {
                lcd.printAt(i + 5, 0, "=");
            } else if(i == progress) {
                lcd.printAt(i + 5, 0, "-" /* loading() */);
            } else if(i > progress) {
                lcd.printAt(i + 5, 0, " ");
            }
        }

    }
}
