#include "myPulse.h"

MyPulse::MyPulse(MyLCD lcd) {
    _lcd = lcd;
    _eeprom = MyEEPROM();

    pinMode((uint8_t)MY_PINS::Power_Sink, OUTPUT);
    pinMode((uint8_t)MY_PINS::Onboard_LED, OUTPUT);

    Pulse pulse = _eeprom.getPulse();
    Status = pulse.Status;
    Type = pulse.Type;
    Interval = pulse.Interval;
}

PulseStatus MyPulse::getStatus() {
    return Status;
}
void MyPulse::setStatus(PulseStatus _Status) {
    Status = _Status;
    put();
}

PulseType MyPulse::getType() {
    return Type;
}
void MyPulse::setType(PulseType _Type) {
    Type = _Type;
    put();
}

long MyPulse::getInterval() {
    return Interval * 100;  // It's saved in 0.1 seconds, not miliseconds.
}
void MyPulse::setInterval(long interval_ms) {
    Interval = interval_ms / 100;
    put();
}

/**
 * Drain enough power now.
 */
void MyPulse::drain() {
    bool wasBacklight = _lcd.isBacklight();

    digitalWrite((uint8_t)MY_PINS::Onboard_LED, HIGH);
    if(Type >= Resistor) {digitalWrite((uint8_t)MY_PINS::Power_Sink, HIGH);}
    if(Type == Both && !wasBacklight) {_lcd.backlight();}
    delay(2);

    digitalWrite((uint8_t)MY_PINS::Onboard_LED, LOW);
    if(Type >= Resistor) {digitalWrite((uint8_t)MY_PINS::Power_Sink, LOW);}
    if(Type == Both && !wasBacklight) {_lcd.noBacklight();}
}

void MyPulse::put() {
    Pulse pulse = {
        Status,
        Type,
        Interval,
    };
    MyEEPROM eeprom;
    eeprom.putPulse(pulse);
}